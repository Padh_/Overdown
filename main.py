import cherrypy
import urllib.request
import os
import threading

global page
global downloadList
global downloadName
global repertory

def openPage(link):
    fi = open(link, "r", encoding="Utf-8")
    try:
        Page = fi.readlines()
    finally:
        fi.close()
    return Page

def start():
    global page
    global repertory
    repertory = input("Quel est le repertoire de stockage :")
    page = openPage("index.html")
        
class master(object):
    def index(self):
        return page
    index.exposed = True
    def addDownload(self, link= None, name=None):
        global downloadList
        global downloadName
        link = str(link)
        name = str(name)
        downloadList.insert(0,link)
        downloadName.insert(0,name)
        return page
    addDownload.exposed = True

class threadDownload(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        global finishDownload
        global downloadList
        while 1:
            if len(downloadList) != 0:
                try :
                    urllib.request.urlretrieve(downloadList[0], str(repertory) + str(downloadName[0]))
                except :
                    print("download fail")
                del downloadList[0]

downloadList = list()
downloadName = list()

try:
    th = threadDownload()
    th.start()
    print("Thread initialisé")
except:
    print("fatal error with threadDownload")

start()
cherrypy.config.update({"tools.staticdir.root":os.getcwd()})
cherrypy.quickstart(master(), config="config.conf")